"use strict"; //strict mode is a way to introduce better error-checking into your code 

/* 

Питання 1. Опишіть своїми словами, що таке метод об'єкту.

Відповідь 1. Методом об`єкту є функція цього об`єкту, це дії, які може виконувати об`єкт. Виступає як властивість об`єкту.   

Питання 2. Який тип даних може мати значення властивості об'єкта?

Відповідь 2. У властивість об`єкту можна вказати дані будь-якого типу, як то число, стрічка, null, булевий тип , функція, масив тощо

Питання 3. Об'єкт це посилальний тип даних. Що означає це поняття?

Відповідь 3. У об'єкті змінні не мають значень, бо вони посилаються на те місце, де ті значення перебувають. 

*/

function createNewUser() {
    const newUser = {
        getLogin: function(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName(_firstName) {
            this.firstName = _firstName;
        },
        setLastName(_lastName) {
            this.lastName = _lastName;
        },
    };

    Object.defineProperties(newUser, {
        firstName: {
            value: prompt("Enter first name:", ""),
            writable: false, //чи можна у властивість щось записувати , зараз заборона 
            configurable: true, //чи можна переналаштувати у минулому , зараз дозволено, керує можливістю змінювати властивість , за дефолтом false
        },

        lastName: {
            value: prompt("Enter last name:", ""),
            writable: false,
            configurable: true,
        },
    });

    return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getLogin());



  